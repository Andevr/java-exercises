package com.company;

public class Main {

    public static void main(String[] args) {
	    isPalindrome(1221);
    }

    public static boolean isPalindrome (int number) {

//        int originalNum = number;
//
//        int reverse = 0;
//        int lastDigit; //this is where the last digit in the number will be stored
//        while(number != 0) {
//            lastDigit = number % 10; //divide the number by 10, the remainder gets stored in the last digit.
//            number = number /10; // divide the number by ten and store the value in here
//            reverse = reverse * 10 + lastDigit; // reverse is a culmination of the other things stored in a variable
//
//        }
//        if (originalNum == reverse) {
//            System.out.println(originalNum + " is palindrome");
//            return true;
//        } else
//        return false;


        int originalNum = number;

        int reverse = 0;
        int lastDigit = 0;// initilaized to avoid error in my printf

        System.out.printf("number\tlastdigit\treverse\n");

        while(number != 0) {
            System.out.printf("%d\t%d\t\t%d\n", number,lastDigit,reverse);
            lastDigit = number % 10;
            reverse = reverse * 10 + lastDigit;
            number = number /10;

        }
        System.out.printf("%d\t%d\t\t%d\n", number,lastDigit,reverse);
        if (originalNum == reverse) {
            System.out.println(originalNum + " is palindrome");
            return true;
        }
        return false;
    }




}
